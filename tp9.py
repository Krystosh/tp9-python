"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

"""
mon_pokedex = [("Bulbizarre", "Plante"), ("Aeromite", "Poison"), ("Abo"
, "Poison")]
"""
def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    res = set()
    for i in range(len(pokedex)) :
        res.add(pokedex[i][1])
    return res
#print(toutes_les_familles(mon_pokedex))

def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    nb_famille = 0
    for pokemon in pokedex :
        if pokemon[1] == famille :
            nb_famille+=1
    return nb_famille
#print(nombre_pokemons(mon_pokedex,'Poison'))

def frequences_famille(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    res = dict()
    for pokemon in pokedex :
        if pokemon[1] not in res.keys() :
            res[pokemon[1]] = 1
        else :
            res[pokemon[1]] += 1
    return res
#print(frequences_famille(mon_pokedex))

def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    res = dict()
    for pokemon in pokedex :
        if pokemon[1] not in res.keys() :
            res[pokemon[1]] = set()
        res[pokemon[1]].add(pokemon[0])
    return res
#print(dico_par_famille(mon_pokedex))

def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    dic = dico_par_famille(pokedex)
    nb = None
    res = None
    for (famille,nb_famille) in dic.items() :
        if res == None :
            nb = nb_famille
            res = famille
        elif nb_famille<nb :
            nb=nb_famille
            res = famille
    return famille
#print(famille_la_plus_representee(mon_pokedex))





# ==========================
# La maison qui rend fou
# ==========================

mqrf1 = {"Abribus":"Astus", "Jeancloddus":"Abribus",
"Plexus":"Gugus", "Astus":None , "Gugus":"Plexus",
"Saudepus":None }
mqrf2 = {"Abribus":"Astus", "Jeancloddus":None ,
"Plexus":"Saudepus", "Astus":"Gugus",
"Gugus":"Plexus", "Saudepus":None }

def quel_guichet(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        str: le nom du guichet qui finit par donner le formulaire A-38
    """
    guichet_courant = guichet
    guicher_prec =''
    tours = 0 
    while guichet_courant != None:
        if tours > len(mqrf) :
            return None
        guicher_prec = guichet_courant
        guichet_courant = mqrf[guichet_courant]
        tours += 1
    return guicher_prec
#print(quel_guichet(mqrf2, "Abribus"))


def quel_guichet_v2(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
    """
    guichet_courant = guichet
    guicher_prec =''
    tours = 0 
    while guichet_courant != None:
        if tours > len(mqrf) :
            return None
        guicher_prec = guichet_courant
        guichet_courant = mqrf[guichet_courant]
        tours += 1
    return (guicher_prec,tours)
#print(quel_guichet_v2(mqrf2, "Abribus"))

def quel_guichet_v3(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
        S'il n'est pas possible d'obtenir le formulaire en partant du guichet de depart,
        cette fonction renvoie None
    """
    guichet_courant = guichet
    guicher_prec =''
    tours = 0 
    while guichet_courant != None:
        if tours > len(mqrf) :
            return None
        guicher_prec = guichet_courant
        guichet_courant = mqrf[guichet_courant]
        tours += 1
    return (guicher_prec,tours)
#print(quel_guichet_v2(mqrf2, "Abribus"))


# ==========================
# Petites bêtes (la suite)
# ==========================

mon_pokedex = {"Bulbizarre":{"Plante", "Poison"}, "Aeromite":{"Poison","Insecte"},
 "Abo":{"Poison"}}


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    res = set()
    for type in pokedex.values() :
        for elem in type :
            res.add(elem)
    return res
#print(toutes_les_familles_v2(mon_pokedex))
def nombre_pokemons_v2(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    nb_pok = 0
    for (pokemon,type) in pokedex.items() :
        if famille in type :
            nb_pok += 1
    return nb_pok
#print (nombre_pokemons_v2(mon_pokedex,'Poison'))
    

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    res = 

def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    ...

def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    ...
